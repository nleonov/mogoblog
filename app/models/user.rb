class User < ApplicationRecord
  devise :database_authenticatable, :registerable

  validates :email,
    presence: true, uniqueness: true, allow_blank: false, case_sensitive: false,
    format: { with: %r'\A(?:[^@\s]+)\@(?:[^@\s]+?)(\.[a-z]{2,})\z'xo }

  validates :password, presence: true
  validates :password, format: {
    with: %r'\A\S{8,}\z',
    message: I18n.t("activerecord.errors.models.user.attributes.password.format")
  }, if: -> { password.present? }

  validate  :matching_password_confirmation, if: -> { password.present? }

  has_many :posts, dependent: :destroy
  has_many :comments, dependent: :destroy

  private

  def matching_password_confirmation
    if password != password_confirmation
      errors.add(:password_confirmation, :confirmation)
    end
  end
end
