FactoryBot.define do
  factory :comment, class: "Comment" do
    user { create(:user) }
    post { create(:post) }
    sequence(:content, 1) { |i| "Comment content - #{i}" }
  end
end
