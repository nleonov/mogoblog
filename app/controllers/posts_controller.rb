class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]

  # GET /posts
  def index
    @posts = Post.all
  end

  # GET /posts/:id
  def show
    @comment = Comment.new(user: current_user)
  end

  # GET /posts/new
  def new
    @post = current_user.posts.new
  end

  # GET /posts/:id/edit
  def edit
    authorize @post

    render :edit
  end

  # POST /posts
  def create
    @post = current_user.posts.new(post_params)

    if @post.save
      redirect_to posts_path, flash: { success: I18n.t("flash.post.create.success") }
    else
      render :new
    end
  end

  # PATCH/PUT /posts/:id
  def update
    authorize @post

    if @post.update(post_params)
      redirect_to @post, flash: { success: I18n.t("flash.post.update.success") }
    else
      render :edit
    end
  end

  # DELETE /posts/:id
  def destroy
    authorize @post

    if @post.destroy
      redirect_to posts_path, flash: { success: I18n.t("flash.post.destroy.success") }
    else
      redirect_to posts_path, flash: { error: I18n.t("flash.post.destroy.fail") }
    end
  end

  private

  def set_post
    @post = Post.find(params[:id])
  end

  def post_params
    params.require(:post).permit(:title, :content)
  end
end
