RSpec.describe RegistrationsController, type: :controller do
  before do
    @request.env["devise.mapping"] = Devise.mappings[:user]
  end

  describe "POST :create" do
    subject(:make_request) { post :create, params: params }

    let(:params) do
      {
        user: {
          email: email,
          password: password,
          password_confirmation: password_confirmation
        }
      }
    end

    context "when email is blank" do
      let(:email) { nil }
      let(:password) { nil }
      let(:password_confirmation) { nil }

      it "renders an error message" do
        expect { make_request }.not_to change { User.all.size }
        expect(session['warden.user.user.key']).to be_nil
      end
    end

    context "when email is present" do
      context "when email is taken" do
        let!(:user) { create(:user) }
        let(:email) { user.email }
        let(:password) { nil }
        let(:password_confirmation) { nil }

        it "renders an error message" do
          expect { make_request }.not_to change { User.all.size }
          expect(session['warden.user.user.key']).to be_nil
        end
      end

      context "when email is not taken" do
        let(:email) { "darth.vader@example.com" }

        context "when password is blank" do
          let(:password) { nil }
          let(:password_confirmation) { nil }

          it "renders an error message" do
            expect { make_request }.not_to change { User.all.size }
            expect(session['warden.user.user.key']).to be_nil
          end
        end

        context "when password is not blank" do
          context "when password is invalid" do
            let(:password) { "123456" }
            let(:password_confirmation) { nil }

            it "renders an error message" do
              expect { make_request }.not_to change { User.all.size }
              expect(session['warden.user.user.key']).to be_nil
            end
          end

          context "when password is valid" do
            let(:password) { "12345678" }

            context "when password_confirmation does not match" do
              let(:password_confirmation) { nil }

              it "renders an error message" do
                expect { make_request }.not_to change { User.all.size }
                expect(session['warden.user.user.key']).to be_nil
              end
            end

            context "when password_confirmation matches" do
              let(:password_confirmation) { "12345678" }

              it "renders an error message" do
                expect { make_request }.to change { User.all.size }.from(0).to(1)
                expect(User.last.email).to eq(email)
                expect(session['warden.user.user.key'].first).to eq([User.last.id])
              end
            end
          end
        end
      end
    end
  end
end
