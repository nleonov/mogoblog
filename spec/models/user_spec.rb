RSpec.describe User, type: :model do
  describe "validations" do
    it { is_expected.to validate_presence_of(:email) }

    context "email uniqueness" do
      subject { build(:user) }

      it { is_expected.to validate_uniqueness_of(:email).case_insensitive }
    end

    context "email format" do
      let(:valid_emails) { ["bob@example.com", "darth.vader@death.star.com"] }
      let(:invalid_emails) { ["@example.com", "darth_vader@death_star", "skywalker@"] }

      it "allows valid emails" do
        valid_emails.each do |valid_email|
          expect(subject).to allow_value(valid_email).for(:email)
        end
      end

      it "does not allow invalid emails" do
        invalid_emails.each do |invalid_email|
          expect(subject).not_to allow_value(invalid_email).for(:email)
        end
      end
    end

    context "password length" do
      context "when validating user with short pass entered" do
        let(:user) { build(:user, password: "abc") }

        it "adds a validation error" do
          expect { user.valid? }.to(
            change { user.errors.messages[:password].present? }.to(true)
          )
        end
      end

      context "when validating user with OK pass entered" do
        let(:user) { build(:user, password: "abcdefgh") }

        it "does not add a validation error" do
          expect { user.valid? }.to_not(
            change { user.errors.messages[:password].present? }
          )
        end
      end
    end

    describe "password confirmation" do
      context "when validating a user with confirmation not matching pass" do
        let(:user) { build(:user, password: "abcdefgh", password_confirmation: "a") }

        it "adds a validation error" do
          expect { user.valid? }.to(
            change { user.errors.messages[:password_confirmation].present? }.to(true)
          )
        end
      end

      context "when validating a user with confirmation matching pass" do
        let(:user) { build(:user, password: "abcdefgh", password_confirmation: "abcdefgh") }

        it "does not add a validation error" do
          expect { user.valid? }.to_not(
            change { user.errors.messages[:password_confirmation].present? }
          )
        end
      end
    end
  end

  describe "associations" do
    it { is_expected.to have_many(:posts).dependent(:destroy) }
    it { is_expected.to have_many(:comments).dependent(:destroy) }
  end
end
