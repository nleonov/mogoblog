RSpec.describe SessionsController, type: :controller do
  before do
    @request.env["devise.mapping"] = Devise.mappings[:user]
  end

  describe "POST :create" do
    subject(:make_request) { post :create, params: params }

    let!(:user) { create(:user, password: "welcome1", password_confirmation: "welcome1") }

    context "when email is invalid" do
      let(:params) { {user: {email: "invalid_email", password: "welcome1"}} }

      it "does not sign in a user and render an error flash message" do
        expect { make_request }.not_to(
          change { session['warden.user.user.key'].try(:first) }
        )

        expect(request.flash[:alert]).to eq("Invalid Email or password.")
        expect(response.status).to eq(200)
      end
    end

    context "when email is valid" do
      context "when password is invalid" do
        let(:params) { {user: {email: user.email, password: nil}} }

        it "does not sign in a user and render an error flash message" do
          expect { make_request }.not_to(
            change { session['warden.user.user.key'].try(:first) }
          )

          expect(request.flash[:alert]).to eq("Invalid Email or password.")
          expect(response.status).to eq(200)
        end
      end

      context "when password is valid" do
        let(:params) { {user: {email: user.email, password: "welcome1"}} }

        it "signs in a user and renders a success flash message" do
          expect { make_request }.to(
            change { session["warden.user.user.key"].try(:first) }.
              from(nil).to([user.id])
          )

          expect(request.flash[:notice]).to eq("Signed in successfully.")
        end

        it "redirects to root path" do
          expect(make_request).to redirect_to(root_path)
        end
      end
    end
  end

  describe "GET :destroy" do
    subject(:make_request) { get :destroy }

    let(:user) { create(:user) }

    before { sign_in(user) }

    it "signs out a user" do
      expect { make_request }.to(
        change { session["warden.user.user.key"].try(:first) }.
          from([user.id]).to(nil)
      )

      expect(request.flash[:notice]).to eq("Signed out successfully.")
    end

    it "redirects to root path" do
      expect(make_request).to redirect_to(root_path)
    end
  end
end
