FactoryBot.define do
  factory :user, class: "User" do
    transient do
      pass "welcome1"
    end

    password { pass }
    password_confirmation { pass }

    sequence(:email, 1) { |i| "email#{i}@example.com" }
  end
end
