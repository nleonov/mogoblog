FactoryBot.define do
  factory :post, class: "Post" do
    user { create(:user) }
    sequence(:title, 1) { |i| "Post Nr.#{i}" }
    content "Post content"
  end
end
