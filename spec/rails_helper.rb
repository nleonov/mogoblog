ENV["RAILS_ENV"] = "test"
require File.expand_path("../../config/environment", __FILE__)

ActiveRecord::Migration.maintain_test_schema!

require "rspec/rails"
require "rails-controller-testing"
require "shoulda/matchers"
require "factory_bot"
require "pundit/rspec"
require "database_cleaner"

FactoryBot.find_definitions

require "spec_helper"

require "simplecov"
SimpleCov.start
