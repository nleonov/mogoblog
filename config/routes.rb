Rails.application.routes.draw do
  devise_for :users,
    controllers: {
      sessions: "sessions",
      registrations: "registrations"
    },
    path_names: {
      sign_in: "login",
      sign_out: "logout",
      registration: "register",
      sign_up: "sign_up"
    }

  root to: "posts#index"

  resources :posts do
    resources :comments, except: [:show]
  end
end
