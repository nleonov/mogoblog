class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.timestamps null: false

      t.string :name, null: false
      t.string :email, null: false
      t.string :encrypted_password, null: false
    end

    add_index :users, :email, unique: true
  end
end
