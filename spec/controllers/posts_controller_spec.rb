RSpec.describe PostsController, type: :controller do
  describe "GET :index" do
    subject(:make_request) { get :index }

    context "when user is not signed in" do
      it { is_expected.to redirect_to("/users/login") }
    end

    context "when user is signed in" do
      let(:user) { create(:user) }
      let(:posts) { controller.instance_variable_get(:@posts) }

      before { sign_in(user) }

      it "collects all Posts and renders :index template" do
        expect(make_request).to render_template("index")
        expect(posts).to eq(Post.all)
      end
    end
  end

  describe "GET :show" do
    subject(:make_request) { get :show, params: {id: post.id} }

    let!(:post) { create_list(:post, 2).last }

    context "when user is not signed in" do
      it { is_expected.to redirect_to("/users/login") }
    end

    context "when user is signed in" do
      let(:user) { post.user }
      let(:post_instance) { controller.instance_variable_get(:@post) }
      let(:comment_instance) { controller.instance_variable_get(:@comment) }

      before { sign_in(user) }

      it "finds a Posts, builds a new Comment and renders :show template" do
        expect(make_request).to render_template("show")
        expect(post_instance).to eq(post)
        expect(comment_instance.user).to eq(user)
        expect(comment_instance).to be_a_new(Comment)
      end
    end
  end

  describe "GET :new" do
    subject(:make_request) { get :new }

    context "when user is not signed in" do
      it { is_expected.to redirect_to("/users/login") }
    end

    context "when user is signed in" do
      let(:user) { create(:user) }
      let(:post_instance) { controller.instance_variable_get(:@post) }

      before { sign_in(user) }

      it "builds a new Post and renders :new template" do
        expect(make_request).to render_template("new")
        expect(post_instance.user).to eq(user)
        expect(post_instance).to be_a_new(Post)
      end
    end
  end

  describe "GET :edit" do
    subject(:make_request) { get :edit, params: {id: post.id} }

    let!(:post) { create(:post) }

    context "when user is not signed in" do
      it { is_expected.to redirect_to("/users/login") }
    end

    context "when user is signed in" do
      before { sign_in(user) }

      context "when post does not belong to user" do
        let(:user) { create(:user) }

        it "rasises a Pundit exception" do
          expect { make_request }.to(
            raise_error(
              Pundit::NotAuthorizedError,
              "not allowed to edit? this #{post.inspect}"
            )
          )
        end
      end

      context "when post belongs to user" do
        let(:user) { post.user }
        let(:post_instance) { controller.instance_variable_get(:@post) }

        it "finds a Posts and renders :edit template" do
          expect(make_request).to render_template("edit")
          expect(post_instance).to eq(post)
        end
      end
    end
  end

  describe "POST :create" do
    subject(:make_request) { post :create, params: params }

    context "when user is not signed in" do
      let(:params) { {} }

      it { is_expected.to redirect_to("/users/login") }
    end

    context "when user is signed in" do
      before { sign_in(user) }

      let(:user) { create(:user) }

      context "when params are invalid" do
        let(:params) { {post: {title: "New title", content: nil}} }

        it "does not create a Post and renders :edit template" do
          expect { make_request }.not_to change { Post.all.size }
          expect(response).to render_template("new")
        end
      end

      context "when create is successful" do
        let(:params) { {post: {title: "New title", content: "New content"}} }
        let(:created_post) { Post.last }

        it "create a Post and redirects to post" do
          expect { make_request }.to change { Post.all.size }.from(0).to(1)
          expect(created_post.title).to eq("New title")
          expect(created_post.content).to eq("New content")
          expect(request.flash[:success]).to eq("Post was successfully created.")
          expect(response).to redirect_to(posts_path)
        end
      end
    end
  end

  describe "POST :update" do
    subject(:make_request) { post :update, params: params }

    let!(:user_post) { create(:post) }

    context "when user is not signed in" do
      let(:params) { {id: user_post.id} }

      it { is_expected.to redirect_to("/users/login") }
    end

    context "when user is signed in" do
      before { sign_in(user) }

      context "when post does not belong to user" do
        let(:user) { create(:user) }
        let(:params) { {id: user_post.id, post: {title: "New title", content: "New content"}} }

        it "rasises a Pundit exception" do
          expect(user_post).not_to receive(:update)

          expect { make_request }.to(
            raise_error(
              Pundit::NotAuthorizedError,
              "not allowed to update? this #{user_post.inspect}"
            )
          )
        end
      end

      context "when post belongs to user" do
        let(:user) { user_post.user }

        context "when update failed" do
          let(:params) { {id: user_post.id, post: {title: "New title", content: nil}} }

          it "does not update a Post and renders :edit template" do
            expect { make_request }.not_to change { user_post.reload.content }
            expect(response).to render_template("edit")
          end
        end

        context "when update is successful" do
          let(:params) { {id: user_post.id, post: {title: "New title", content: "New content"}} }

          it "updates a Post and redirects to post" do
            expect { make_request }.to(
              change { user_post.reload.title }.from(user_post.title).to("New title").
              and change { user_post.reload.content }.from(user_post.content).to("New content")
            )

            expect(request.flash[:success]).to eq("Post was successfully updated.")
            expect(response).to redirect_to(user_post)
          end
        end
      end
    end
  end

  describe "DELETE :destroy" do
    subject(:make_request) { delete :destroy, params: {id: post.id} }

    let!(:post) { create(:post) }

    context "when user is not signed in" do
      it { is_expected.to redirect_to("/users/login") }
    end

    context "when user is signed in" do
      before { sign_in(user) }

      context "when post does not belong to user" do
        let(:user) { create(:user) }

        it "rasises a Pundit exception" do
          expect(post).not_to receive(:update)

          expect { make_request }.to(
            raise_error(
              Pundit::NotAuthorizedError,
              "not allowed to destroy? this #{post.inspect}"
            )
          )
        end
      end

      context "when post belongs to user" do
        let(:user) { post.user }

        context "when destroy failed" do
          before do
            allow(Post).to receive(:find).with(post.id.to_s).and_return(post)
            allow(post).to receive(:destroy).and_return(false)
          end

          it "does not deletes a Post and redirects to post index" do
            expect { make_request }.not_to change { Post.all.size }
            expect(request.flash[:error]).to eq("Post was not deleted.")
            expect(response).to redirect_to(posts_path)
          end
        end

        context "when destroy is successful" do
          it "deletes a Post and and redirects to posts index" do
            expect { make_request }.to change { Post.all.size }.from(1).to(0)
            expect(request.flash[:success]).to eq("Post was successfully deleted.")
            expect(response).to redirect_to(posts_path)
          end
        end
      end
    end
  end
end
