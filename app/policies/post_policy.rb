class PostPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      user.posts
    end
  end

  def scope
    Pundit.policy_scope!(user, record.class)
  end

  def edit?
    scope.where(id: record.id).exists?
  end

  def update?
    edit?
  end

  def destroy?
    scope.where(id: record.id).exists?
  end
end
