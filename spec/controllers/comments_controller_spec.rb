RSpec.describe CommentsController, type: :controller do
  describe "GET :edit" do
    subject(:make_request) do
      get :edit, params: {post_id: comment.post_id, id: comment.id}
    end

    let!(:comment) { create(:comment) }

    context "when user is not signed in" do
      it { is_expected.to redirect_to("/users/login") }
    end

    context "when user is signed in" do
      before { sign_in(user) }

      context "when comment does not belong to user" do
        let(:user) { create(:user) }

        it "rasises a Pundit exception" do
          expect { make_request }.to(
            raise_error(
              Pundit::NotAuthorizedError,
              "not allowed to edit? this #{comment.inspect}"
            )
          )
        end
      end

      context "when comment belongs to user" do
        let(:user) { comment.user }

        let(:comment_instance) { controller.instance_variable_get(:@comment) }

        it "finds a Comment and renders :edit template" do
          expect(make_request).to render_template("edit")
          expect(comment_instance).to eq(comment)
        end
      end
    end
  end

  describe "POST :create" do
    subject(:make_request) { post :create, params: params }

    let!(:user_post) { create(:post) }

    context "when user is not signed in" do
      let(:params) { {post_id: user_post.id} }

      it { is_expected.to redirect_to("/users/login") }
    end

    context "when user is signed in" do
      before { sign_in(user) }

      let(:user) { create(:user) }

      context "when params are invalid" do
        let(:params) { {post_id: user_post.id, comment: {content: nil}} }

        it "does not update a Comment and redirects to comments post" do
          expect { make_request }.not_to change { Comment.all.size }
          expect(request.flash[:error]).to eq("Comment was not created.")
          expect(response).to redirect_to(user_post)
        end
      end

      context "when create is successful" do
        let(:params) { {post_id: user_post.id, comment: {content: "New content"}} }
        let(:created_comment) { Comment.last }

        it "updates a Comment and redirects to comments post" do
          expect { make_request }.to change { Comment.all.size }.from(0).to(1)
          expect(created_comment.content).to eq("New content")
          expect(request.flash[:success]).to eq("Comment was successfully created.")
          expect(response).to redirect_to(created_comment.post)
        end
      end
    end
  end

  describe "POST :update" do
    subject(:make_request) { post :update, params: params }

    let!(:comment) { create(:comment) }

    context "when user is not signed in" do
      let(:params) { {post_id: comment.post_id, id: comment.id} }

      it { is_expected.to redirect_to("/users/login") }
    end

    context "when user is signed in" do
      before { sign_in(user) }

      context "when comment does not belong to user" do
        let(:user) { create(:user) }

        let(:params) do
          {
            post_id: comment.post_id,
            id: comment.id,
            comment: {
              content: "New content"
            }
          }
        end

        it "rasises a Pundit exception" do
          expect(comment).not_to receive(:update)

          expect { make_request }.to(
            raise_error(
              Pundit::NotAuthorizedError,
              "not allowed to update? this #{comment.inspect}"
            )
          )
        end
      end

      context "when comment belongs to user" do
        let(:user) { comment.user }

        context "when update failed" do
          let(:params) do
            {
              post_id: comment.post_id,
              id: comment.id,
              comment: {
                content: nil
              }
            }
          end

          it "does not update a Comment and renders :edit template" do
            expect { make_request }.not_to change { comment.reload.content }
            expect(response).to render_template("edit")
          end
        end

        context "when update is successful" do
          let(:params) do
            {
              post_id: comment.post_id,
              id: comment.id,
              comment: {
                content: "New content"
              }
            }
          end

          it "updates a Comment and redirects to comments post" do
            expect { make_request }.to(
              change { comment.reload.content }.from(comment.content).to("New content")
            )

            expect(request.flash[:success]).to eq("Comment was successfully updated.")
            expect(response).to redirect_to(comment.post)
          end
        end
      end
    end
  end

  describe "DELETE :destroy" do
    subject(:make_request) do
      delete :destroy, params: {post_id: comment.post_id, id: comment.id}
    end

    context "when user is not signed in" do
      let!(:comment) { create(:comment) }

      it { is_expected.to redirect_to("/users/login") }
    end

    context "when user is signed in" do
      before { sign_in(user) }

      context "when comment does not belong to user" do
        let(:user) { create(:user) }

        context "when comment does not belong to users post" do
          let!(:comment) { create(:comment) }

          it "rasises a Pundit exception" do
            expect { make_request }.to(
              raise_error(
                Pundit::NotAuthorizedError,
                "not allowed to destroy? this #{comment.inspect}"
              )
            )
          end
        end

        context "when comment belongs to users post" do
          let(:post) { create(:post, user: user) }
          let!(:comment) { create(:comment, post: post) }

          it "deletes a Comment and redirects coomments post" do
            expect { make_request }.to change { Comment.all.size }.from(1).to(0)
            expect(request.flash[:success]).to eq("Comment was successfully deleted.")
            expect(response).to redirect_to(post)
          end
        end
      end

      context "when comment belongs to user" do
        let(:user) { create(:user) }
        let!(:comment) { create(:comment, user: user) }
        let!(:post) { comment.post }

        context "when destroy failed" do
          before do
            allow(Comment).to receive(:find).with(comment.id.to_s).and_return(comment)
            allow(comment).to receive(:destroy).and_return(false)
          end

          it "does not delete a Comment and redirects to comments post" do
            expect { make_request }.not_to change { Comment.all.size }
            expect(request.flash[:error]).to eq("Comment was not deleted.")
            expect(response).to redirect_to(post)
          end
        end

        context "when destroy is successful" do
          it "deletes a Comment and redirects comments post" do
            expect { make_request }.to change { Comment.all.size }.from(1).to(0)
            expect(request.flash[:success]).to eq("Comment was successfully deleted.")
            expect(response).to redirect_to(post)
          end
        end
      end
    end
  end
end
