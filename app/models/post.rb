class Post < ApplicationRecord
  validates :user_id, :title, :content, presence: true

  belongs_to :user
  has_many :comments, dependent: :destroy
end
