class CommentsController < ApplicationController
  before_action :set_post, only: [:edit, :create, :update, :destroy]
  before_action :set_comment, only: [:edit, :update, :destroy]

  # GET /posts/:post_id/comments/:id/edit
  def edit
    authorize @comment

    render :edit
  end

  # POST /posts/:post_id/comments
  def create
    @comment = @post.comments.create(comment_params.merge(user_id: current_user.id))

    if @comment.save
      redirect_to @post, flash: { success: I18n.t("flash.comment.create.success") }
    else
      redirect_to @post, flash: { error: I18n.t("flash.comment.create.fail") }
    end
  end

  # PATCH/PUT /posts/:post_id/comments/:id
  def update
    authorize @comment

    if @comment.update(comment_params)
      redirect_to @post, flash: { success: I18n.t("flash.comment.update.success") }
    else
      render :edit
    end
  end

  # DELETE /posts/:post_id/comments/:id
  def destroy
    authorize @comment

    if @comment.destroy
      redirect_to @post, flash: { success: I18n.t("flash.comment.destroy.success") }
    else
      redirect_to @post, flash: { error: I18n.t("flash.comment.destroy.fail") }
    end
  end

  private

  def set_post
    @post ||= Post.find(params[:post_id])
  end

  def set_comment
    @comment ||= Comment.find(params[:id])
  end

  def comment_params
    params.require(:comment).permit(:content)
  end
end
