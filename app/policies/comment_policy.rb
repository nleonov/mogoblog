class CommentPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      OpenStruct.new(
        posts: user.posts,
        comments: user.comments
      )
    end
  end

  def scope
    Pundit.policy_scope!(user, record.class)
  end

  def edit?
    scope.comments.where(id: record.id).exists?
  end

  def update?
    edit?
  end

  def destroy?
    scope.comments.where(id: record.id).exists? ||
      scope.posts.where(id: record.post_id).exists?
  end
end
